<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 10:00
 *  File: status.php
 */
declare (strict_types=1);

return[
    'success'			=>	0,		//成功
    'padding'			=>	1,		//待审核
    'failed'			=>	2,		//失败
    'goto'			=>	3,		//跳转
    'not_found'			=>	4,		//404
    'token_timeout'		=>	5,		//token过期
    'not_login'			=>	-1,		//没有登录
    'user_is_register'	=>	-2,		//用户已经注册

    //mysql状态配置
    'mysql'	=>	[
        "table_normal"	=>	0,		//正常
        "table_padding"	=>	1,		//待审核
        "table_delete"	=>	99,		//删除
    ]
];