<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 14:26
 *  File: redis.php
 */
declare (strict_types=1);

return[

    //登录token
    "token_pre"         => "luck_access_token_pre__",
    //登录token保存时间(一天)
    "token_expire"      => 24*3600,
];