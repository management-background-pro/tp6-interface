<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 10:01
 *  File: message.php
 */
declare (strict_types=1);

return [
    //执行成功
    'success'       => "执行成功",
    //执行失败
    'failed'       => "执行失败",
    //跳转
    'goto'       => "跳转",

];