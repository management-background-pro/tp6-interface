<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 14:36
 *  File: Index.php
 */
declare (strict_types=1);

namespace app\api\controller;

use app\BaseController;
use think\annotation\Route;
use think\annotation\route\Group;
use think\exception\HttpException;

/**
 * @Group("api")
 */
class Index extends BaseController
{
    /**
     * 数据名称
     * @return mixed
     * @Route("x",method="GET")
     */
    public function index(): object
    {
        return $this->success(['id'=>2]);
    }

    /**
     * @param  string $name 数据名称
     * @return mixed
     * @Route("hello/:name")
     */
    public function hello(string $name): object
    {
        return $this->fail($name);
    }
}