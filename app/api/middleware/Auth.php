<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-19
 *  Time: 15:07
 *  File: Auth.php
 */
declare (strict_types=1);

namespace app\api\middleware;

use app\Request;
use think\Response;

class Auth
{
    public function handle(Request $request, \Closure $next)
    {
        //前置中间件
        $response = $next($request);
//        //后置中间件
//        $this->end($response);
        return $response;
    }

    /**
     * 中间件结束调度
     * @param Response $response
     * @return void
     */
    public function end(Response $response)
    {
        
    }
}