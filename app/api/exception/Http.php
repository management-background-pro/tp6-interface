<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 17:06
 *  File: ExceptionHandle.php
 */
declare (strict_types=1);

namespace app\api\exception;

use think\exception\Handle;
use think\Response;
use Throwable;

class Http extends Handle
{
    public $httpStatus = 501;

    /**
     * Render an exception into an Http response.
     *
     * @access public
     * @param $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response{
        $msg = $e->getMessage();
        $status = config('status.failed');

        if($msg == config('status.goto')){
            $status = config('status.goto');
        }

        $httpStatus = $this->httpStatus;
        // 添加自定义异常处理机制
        if(method_exists($e, "getStatusCode")){
            $httpStatus = $e->getStatusCode();
        }

        return show($status, $msg, ['4'], $httpStatus);
    }
}