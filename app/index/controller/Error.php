<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-19
 *  Time: 10:36
 *  File: Error.php
 */
declare (strict_types=1);

namespace app\index\controller;

use app\BaseController;

class Error extends BaseController
{
    public function __call($method, $args):object
    {
        // TODO: Implement __call() method.
        return $this->fail('找不到你的请求!');
    }
}