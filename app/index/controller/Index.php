<?php
namespace app\index\controller;

use app\BaseController;
use think\annotation\Route;

class Index extends BaseController
{
    /**
     * 数据名称
     * @return mixed
     * @Route("/x",method="GET")
     */
    public function index(): object
    {
        return $this->success(['id'=>1]);
    }

    /**
     * @param  string $name 数据名称
     * @return mixed
     * @Route("/hello3/:name")
     */
    public function hello(string $name): object
    {
        return $this->fail($name);
    }
}
