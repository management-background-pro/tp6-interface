<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 14:12
 *  File: Str.php
 */
declare (strict_types=1);

namespace app\common\lib\tools;

class Str
{
    /**
     * 返回字符串token
     * @param string $str   加密字符串
     * @return string
     */
    public function token(string $str):string
    {
        //TRUE - 原始 16 字符二进制格式, FALSE - 默认。32 字符十六进制数
        $tokenSalt = md5(uniqid(md5(microtime(true))), true);

        return sha1($tokenSalt . $str);
    }

    /**
     * 生成盐
     * @param int $bit  生成盐的长度
     * @return string
     */
    public function salt(int $bit):string
    {
        //盐字符集
        $chats = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $str = "";
        for ($i = 0; $i < $bit; $i++){
            $str .= substr($chats, mt_rand(0, strlen($chats)-1), 1);
        }
        return $str;
    }
}