<?php
/**
 *  Created by: PhpStorm
 *  Author: 式神(vashi)<289650682@qq.com>
 *  Date: 2022-05-18
 *  Time: 10:24
 *  File: Redis.php
 */
declare (strict_types=1);

namespace app\common\lib\tools;

use Psr\SimpleCache\InvalidArgumentException;
use think\facade\Cache;

class Redis
{
    private $store = null;

    public function __construct($store = 'redis')
    {
        $this->setStore($store);
    }

    /**
     * @param null $store
     */
    protected function setStore($store): Redis
    {
        $this->store = $store;
        return $this;
    }

    /**
     * @param string $key       缓存变量名
     * @param mixed $value      存储数据
     * @param int|null $ttl     有效时间（秒）
     * @return bool
     * @throws InvalidArgumentException
     */
    public function set(string $key,$value,int $ttl=null): bool
    {
        return Cache::store($this->store)->set($key, $value, $ttl);
    }

    /**
     * @param string $key       缓存变量名
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function get(string $key)
    {
        return Cache::store($this->store)->get($key);
    }

    /**
     * @param string $key
     * @return bool
     * @throws InvalidArgumentException
     */
    public function delete(string $key):bool
    {
        return Cache::store($this->store)->delete($key);
    }

    /**
     * 清除缓存
     * @access public
     * @return bool
     */
    public function clear(): bool{
        return Cache::store($this->store)->clear();
    }

    /**
     * 自增缓存（针对数值缓存）
     * @access public
     * @param  string    $key 缓存变量名
     * @param  int       $step 步长
     * @return false|int
     */
    public function inc(string $key, int $step = 1){
        return Cache::store($this->store)->inc($key, $step);
    }

    /**
     * 自减缓存（针对数值缓存）
     * @access public
     * @param  string    $key 缓存变量名
     * @param  int       $step 步长
     * @return false|int
     */
    public function dec(string $key, int $step = 1){
        return Cache::store($this->store)->dec($key, $step);
    }
}