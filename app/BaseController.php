<?php
declare (strict_types = 1);

namespace app;

use app\common\lib\tools\Redis;
use Psr\SimpleCache\InvalidArgumentException;
use think\App;
use think\exception\ValidateException;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];

    protected $redis = null;

    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app      = $app;
        $this->request  = $this->app->request;

        $this->redis    = new Redis();

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {}

    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        return $v->failException(true)->check($data);
    }

    /**
     * json数据反馈
     * @param int $status	返回状态码（config状态码）
     * @param string $message	返回提示
     * @param $data	返回data数据
     * @return object 返回json字符串
     */
    public function show(int $status,string $message,$data=[]):object
    {
        return show($status, $message, $data);
    }
    
    /**
     * 返回成功
     * @param array $data	返回data数据
     * @return object 返回json字符串
     */
    public function success(array $data):object
    {
        return $this->show(
            config('status.success'),
            config('message.success'),
            $data
        );
    }

    /**
     * 返回失败
     * @param string $message   错误的提示
     * @return object 返回json字符串
     */
    public function fail(string $message):object
    {
        return $this->show(
            config('status.failed'),
            $message,
            null
        );
    }

    /**
     * 获取token数据
     * @param string $name  access-token | refresh-token
     * @return string   返回token数据
     */
    public function getToken(string $name='access-token'):string{
        return $this->request->header($name);
    }

    /**
     * 返回登录用户信息
     * @return mixed
     * @throws InvalidArgumentException
     */
    public function getUser():mixed{
        return $this->redis->get(config('redis.token_pre') . $this->getToken());
    }

    /**
     * 返回登录用户id
     * @return int
     * @throws InvalidArgumentException
     */
    public function getUid():int
    {
        return $this->getUser()['id'];
    }

    /**
     * 空控制器
     * @param $method
     * @param $args
     * @return object
     */
    public function __call($method, $args):object
    {
        return $this->fail('找不到你的请求!');
    }
}
