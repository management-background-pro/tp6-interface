<?php
// 应用公共文件
/**
 * @param int $status	返回状态码（config状态码）
 * @param string $message	返回提示
 * @param mixed $data	返回data数据
 * @param int $httpStatus	http状态
 * @return object 返回json字符串
 */
function show(int $status,string $message,$data=[],int $httpStatus=200):object{
    $result = [
        'code'	=>	$status,
        'msg'	=>	$message,
        'data'	=>	$data
    ];
    return json($result,$httpStatus);
}